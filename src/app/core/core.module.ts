import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IpfsService } from './services/ipfs/ipfs.service';
import { OrbitDbService } from './services/ipfs/orbit-db.service';
import { ElectronService } from './services';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [IpfsService, OrbitDbService, ElectronService]
})
export class CoreModule { }
