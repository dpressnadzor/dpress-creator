import { CommonModule } from "@angular/common";
import { ElectronService } from "../electron/electron.service";
import { OrbitDbService } from "../ipfs/orbit-db.service";

export const getTestingModule = () => ({
  declarations: [],
  imports: [CommonModule],
  providers: [ElectronService, OrbitDbService],
});
