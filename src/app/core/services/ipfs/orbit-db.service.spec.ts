import { TestBed } from '@angular/core/testing';
import { getTestingModule } from '../test/getTestingModule';
import { OrbitDbService } from './orbit-db.service';

describe('OrbitDbService', () => {
  let service: OrbitDbService;

  beforeEach(() => {
    TestBed.configureTestingModule(getTestingModule());
    service = TestBed.inject(OrbitDbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
