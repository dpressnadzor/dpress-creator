import { Injectable } from '@angular/core';
import { ElectronService } from '..';
import { APP_CONFIG } from '../../../../environments/environment';

export interface OrbitDbInited {
  address: string;
  identity: string;
  own: boolean;
  instance: any;
}

export interface ArticleDTO {
  _id: string;
  thumbnail: string;
  createdAt: number;
  title: string;
  html: string;
}

@Injectable()
export class OrbitDbService {
  orbitStore = null;
  constructor(private electronService: ElectronService) {}
  async init(): Promise<void> {
    // init docstore
    const orbitInstance = await this.electronService.orbitDb.getOrbitInstance();
    this.orbitStore = await orbitInstance.docstore(
      APP_CONFIG.rootAddress,
      {
        create: true,
        sync: false
      }
    );
  }
}
